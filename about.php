<?php

/*

	Template Name: About

*/

get_header(); ?>

	<?php get_template_part('template-parts/about/page-header'); ?>

	<?php get_template_part('template-parts/about/main'); ?>

	<?php get_template_part('template-parts/about/jobs'); ?>

<?php get_footer(); ?>