<?php

function bearsmith_mailchimp_subscriber_status( $email, $status, $list_id, $api_key ){
	$data = array(
		'apikey'        => $api_key,
    	'email_address' => $email,
		'status'        => $status,
	);
	$mailchimp_api = curl_init(); // initialize cURL connection
 
	curl_setopt($mailchimp_api, CURLOPT_URL, 'https://' . substr($api_key,strpos($api_key,'-')+1) . '.api.mailchimp.com/3.0/lists/' . $list_id . '/members/' . md5(strtolower($data['email_address'])));
	curl_setopt($mailchimp_api, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.base64_encode( 'user:'.$api_key )));
	curl_setopt($mailchimp_api, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
	curl_setopt($mailchimp_api, CURLOPT_RETURNTRANSFER, true); // return the API response
	curl_setopt($mailchimp_api, CURLOPT_CUSTOMREQUEST, 'PUT'); // method PUT
	curl_setopt($mailchimp_api, CURLOPT_TIMEOUT, 10);
	curl_setopt($mailchimp_api, CURLOPT_POST, true);
	curl_setopt($mailchimp_api, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($mailchimp_api, CURLOPT_POSTFIELDS, json_encode($data) ); // send data in json
 
	$result = curl_exec($mailchimp_api);
	return $result;
}


function bearmsith_mailchimp_subscribe(){
	$list_id = '0cff881ecf';
	$api_key = '5553881fcf4cc5f429757cdc5b84fbad-us19';
	$result = json_decode( bearsmith_mailchimp_subscriber_status($_POST['email'], 'subscribed', $list_id, $api_key ) );
	// print_r( $result ); 
	if( $result->status == 400 ){
		foreach( $result->errors as $error ) {
			echo '<p>Error: ' . $error->message . '</p>';
		}
	} elseif( $result->status == 'subscribed' ){
		echo 'Thank you, ' . $result->merge_fields->FNAME . '. You have subscribed successfully';
	}
	// $result['id'] - Subscription ID
	// $result['ip_opt'] - Subscriber IP address
	die;
}
 
add_action('wp_ajax_mailchimpsubscribe','bearmsith_mailchimp_subscribe');
add_action('wp_ajax_nopriv_mailchimpsubscribe','bearmsith_mailchimp_subscribe');