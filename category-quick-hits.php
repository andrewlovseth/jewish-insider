<?php get_header(); ?>

	<section class="main">			
		<?php get_template_part('template-parts/global/sidebar'); ?>

		<section class="article-teasers">		
			<div class="section-header mobile">
				<h2><a href="<?php echo site_url('/topics/quick-hits/'); ?>">Quick Hits</a></h2>
			</div>

			<?php if ( have_posts() ): ?>
				<?php while ( have_posts() ): the_post(); ?>

					<?php get_template_part('alm_templates/pod'); ?>	

				<?php endwhile; ?>

				<?php
					the_posts_pagination(
						array(
							'mid_size'  => 1,
							'prev_text' => __('Prev'),
							'next_text' => __('Next'),
						)
					);
				?>
			<?php endif; ?>
		</section>

	</section>
	
<?php get_footer(); ?>