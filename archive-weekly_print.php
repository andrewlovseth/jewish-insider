<?php get_header(); ?>

    <main class="site-content grid">
        <section class="page-header">
            <h1 class="x-large-title">The Weekly Print</h1>

            <div class="copy p2">
                <?php the_field('weekly_print_copy', 'options'); ?>
            </div>
        </section>

        <section class="weekly-grid">

            <?php
                $args = array(
                    'post_type' => 'weekly_print',
                    'posts_per_page' => -1
                );
                $query = new WP_Query( $args );
                if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
                

                <div class="item">
                    <a href="<?php the_field('pdf'); ?>" rel="external"><?php the_time('F j, Y'); ?></a>
                </div>

            <?php endwhile; endif; wp_reset_postdata(); ?>
        
        </section>
    </main>
        
<?php get_footer(); ?>