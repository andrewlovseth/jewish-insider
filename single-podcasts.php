<?php

$show_array = wp_get_post_terms( $post->ID, 'show', array( 'fields' => 'all' ) );
$show = $show_array[0];
$show_title = $show->name;
$show_description = $show->description;
$show_link = get_term_link($show->slug, 'show');

$hosts = get_field('hosts', $show);
$show_art = get_field('show_art', $show);

get_header(); ?>

    <main class="site-content">
        <?php 
            $args = array(
                'show' => $show,
                'show_title' => $show_title,
                'show_link' => $show_link
            );
            get_template_part('template-parts/single-podcasts/subscribe', null, $args);           
        ?>

        <?php get_template_part('template-parts/single-podcasts/episode-header'); ?>

        <?php get_template_part('template-parts/single-podcasts/podcast-player'); ?>



        <section class="podcast-details grid">
            <?php get_template_part('template-parts/single-podcasts/show-notes'); ?>

            <?php 
                $args = array(
                    'show' => $show,
                    'show_description' => $show_description
                );
                get_template_part('template-parts/single-podcasts/about-the-podcast', null, $args);           
            ?>
        </section>

    </main>

<?php get_footer(); ?>