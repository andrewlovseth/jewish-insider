<?php

/*
 * Daily Kickoff Section Block Template
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'dk-section-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'dk-section dk-block wp-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
if( $is_preview ) {
    $className .= ' is-admin';
}

$header_image = get_field('section_header_image');

?>

<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <div class="section-header" data-header-image="<?php echo $header_image['url']; ?>">
        <h2><?php the_field('section_header'); ?></h2>
    </div>

    <div class="content">
        <InnerBlocks />
    </div>    
</section>