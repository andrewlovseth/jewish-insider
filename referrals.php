<?php

/*

	Template Name: Referrals

*/

get_header(); ?>

	<section class="main">			

		<section id="subscribe" class="subscribe page-content">
			<?php get_template_part('template-parts/referral/info'); ?>
		
			<?php get_template_part('template-parts/referral/preview'); ?>
		</section>

	</section>

<?php get_footer(); ?>