<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="google-site-verification" content="p_yKTYgn72OnvCEQ5ApWHAh8tuEXW1VI6VUkVMVp_oE" />
	
	<?php wp_head(); ?>

	<?php get_template_part('template-parts/header/twitter-meta-tags'); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

	<?php 
		global $theme;
	?>

	<?php get_template_part('template-parts/header/navigation'); ?>
	
	<div id="page">

		<?php get_template_part('template-parts/header/network'); ?>

		<header class="site-header<?php if($theme == 'overlay'): ?> header-overlay<?php endif; ?>">
			<div class="wrapper">

				<?php get_template_part('template-parts/header/logo'); ?>

				<?php get_template_part('template-parts/header/desktop-nav-links'); ?>

				<div class="utilities">
					<?php get_template_part('template-parts/header/social'); ?>

					<?php get_template_part('template-parts/header/subscribe'); ?>

					<?php get_template_part('template-parts/header/search'); ?>

					<?php get_template_part('template-parts/header/hamburger-menu'); ?>			
				</div>
				
			</div>
		</header>