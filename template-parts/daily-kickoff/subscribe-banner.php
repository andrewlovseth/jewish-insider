<section class="subscribe">
    <div class="wrapper">

        <div class="info">
            <?php $homepage = get_option('page_on_front'); ?>

            <div class="headline">
                <h3><?php the_field('subscribe_headline', $homepage ); ?></h1>
            </div>
        
            <div class="cta">
                <a href="#" class="subscribe-btn subscribe-trigger">Subscribe</a>
            </div>
        </div>

        <div class="photo">
            <img src="<?php bloginfo('template_directory') ?>/images/phone-preview-small.png" alt="" />
        </div>

    </div>
</section>