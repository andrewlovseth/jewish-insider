<?php if(get_field('featured_image')): ?>

	<section class="quick-hit-photo">
		<div class="photo-wrapper">
			<?php $image = get_field('featured_image'); if( $image ): ?>
				<?php echo wp_get_attachment_image($image['ID'], 'large'); ?>
			<?php endif; ?>
			
			<?php get_template_part('template-parts/global/photo-credit'); ?>
		</div>

		<?php if($image['caption']): ?>
			<div class="caption">
				<p><?php echo $image['caption']; ?></p>
			</div>
		<?php endif; ?>	
	</section>

<?php endif; ?>

