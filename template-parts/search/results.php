<section class="results">
    <div class="wrapper">
    
        <?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

            <?php get_template_part('template-parts/global/search-result'); ?>

        <?php endwhile; endif; ?>
    
        <?php
            the_posts_pagination(
                array(
                    'mid_size'  => 1,
                    'prev_text' => __('Prev'),
                    'next_text' => __('Next'),
                )
            );
        ?>

    </div>
</section>