<section class="jobs">
    <div class="wrapper">

        <div class="section-header">
            <h3><?php the_field('jobs_headline'); ?></h3>

            <div class="copy p2">
                <?php the_field('jobs_deck'); ?>
            </div>
        </div>

        <?php $posts = get_field('jobs'); if( $posts ): ?>

            <div class="job-listings">
                <?php foreach( $posts as $p ): ?>
                    <article class="job">
                        <div class="meta">
                            <div class="category">
                                <span><?php the_field('category', $p->ID); ?></span>
                            </div>

                            <div class="location">
                                <span><?php the_field('location', $p->ID); ?></span>
                            </div>
                        </div>

                        <div class="headline">
                            <h4><a href="<?php echo get_permalink( $p->ID ); ?>"><?php echo get_the_title( $p->ID ); ?></a></h4>
                        </div>

                        <div class="copy p2">
                            <?php the_field('short_description', $p->ID); ?>
                        </div>

                        <div class="read-more">
                            <a href="<?php echo get_permalink( $p->ID ); ?>">Read More</a>
                        </div>

                    </article>
                <?php endforeach; ?>						
            </div>

        <?php else: ?>

            <div class="job-listings no-jobs">
                <article class="job">
                    <div class="headline">
                        <h4>No jobs at the moment.</h4>
                    </div>
                </article>
            </div>

        <?php endif; ?>			

    </div>
</section>