<?php

$profile = get_field('profile');
$photo = $profile['photo'];
$tagline = $profile['tagline'];
$designation = $profile['designation'];
$key_cat = $profile['key_category'];

$editorial = get_field('editorial');
$link = $editorial['primary_article'];
$secondary_articles = $editorial['secondary_articles'];

$locations = get_the_terms( $post->ID, 'location' );
$location = $locations[0]->name;

if($key_cat) {
    $cat = $key_cat->name;
} else {
    $cats = get_the_terms( $post->ID, 'filters' );
    $cat = $cats[0]->name;
}

?>

<section class="profile grid">

    <div class="profile-photo">
        <div class="photo <?php echo $designation; ?>">
            <div class="cat">
                <h4><?php echo $cat; ?></h4>
            </div>

            <div class="content">
                <img src="<?php echo $photo['sizes']['thumbnail']; ?>" alt="<?php echo $photo['alt']; ?>" />
            </div>
        </div>
    </div>

    <div class="info">
        <div class="headline name">
            <div class="location">
                <h4><?php echo $location; ?></h4>
            </div>

            <h1><?php the_title(); ?></h1>

            <div class="tagline">
                <h2><?php echo $tagline; ?></h2>
            </div>
        </div>

        <div class="articles">
            <?php if( $link ): ?>
                <?php 
                    $args = array(
                        'link' => $link,
                    );
                    get_template_part('template-parts/single-profiles/teaser', null, $args);           
                ?>
            <?php endif; ?>

            <?php if($secondary_articles):?>

                <?php foreach($secondary_articles as $secondary_article): ?>
                    <?php

                        $args = array(
                            'link' => $secondary_article,
                        );
                        get_template_part('template-parts/single-profiles/teaser', null, $args);           
                    ?>
                <?php endforeach; ?>

            <?php endif; ?>

        </div>

    </div>
</section>