<section class="results">
    <div class="wrapper">

        <?php
            $author_posts = get_posts(array(
                'post_type' => 'post',
                'posts_per_page' => -1,
                'meta_query' => array(
                    array(
                        'key' => 'authors',
                        'value' => '"' . get_the_ID() . '"',
                        'compare' => 'LIKE'
                    )
                )
            ));

            $author_post_array = array();
            foreach($author_posts as $author_post) {
                array_push($author_post_array, $author_post->ID);
            }

            $author_post_ids = implode(', ', $author_post_array);

            if($author_post_ids != ''): ?>

                <div class="section-header headline">
                    <h2>Recent Articles by <?php the_title(); ?></h2>
                </div>

                <?php echo do_shortcode('[ajax_load_more container_type="div" post_type="post" posts_per_page="10" post__in="' . $author_post_ids .'" scroll="false"]'); ?>

            <?php endif ; ?>

    </div>
</section>