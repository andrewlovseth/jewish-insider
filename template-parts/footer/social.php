<div class="social">
    <div class="header">
        <h5>Social</h5>
    </div>

    <div class="links">
        <div class="social-link facebook">
            <a href="<?php the_field('facebook', 'options'); ?>" rel="external">
                <img src="<?php bloginfo('template_directory') ?>/images/facebook-icon-white.svg" alt="Facebook">
            </a>
        </div>

        <div class="social-link twitter">
            <a href="<?php the_field('twitter', 'options'); ?>" rel="external">
                <img src="<?php bloginfo('template_directory') ?>/images/twitter-icon-white.svg" alt="Twitter">
            </a>
        </div>

        <div class="social-link flipboard">
            <a href="<?php the_field('flipboard', 'options'); ?>" rel="external">
                <img src="<?php bloginfo('template_directory') ?>/images/flipboard-icon-white.svg" alt="Flipboard">
            </a>
        </div>
    </div>
</div>