<?php

    $banner = get_field('banner');
    $link = $banner['link'];
    $graphic = $banner['graphic'];

	if( $link ): 
	$link_url = $link['url'];
	$link_title = $link['title'];
	$link_target = $link['target'] ? $link['target'] : '_self';
 ?>

    <div class="banner home-banner"> 	
        <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">

            <?php if( $graphic ): ?>
                <?php echo wp_get_attachment_image($graphic['ID'], 'large'); ?>
            <?php endif; ?>
        </a>
    </div>

<?php endif; ?>
