<section id="email-gateway">
	<div class="overlay">
		<div class="overlay-wrapper">
		
			<a href="#" class="email-gateway-close"></a>

			<div class="info">
				<div class="headline">
					<h3>Enter your email address to continue reading</h3>
				</div>

				<div class="dek">
					<p></p>
				</div>

				<form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" id="mailchimp-gateway">
                    <input type="email" name="email" placeholder="Email Address" required />
                    <input type="hidden" name="action" value="mailchimpsubscribe" />              
                    <button class="subscribe-btn clear-charcoal">Sign Up</button>
                </form>			
			</div>

		</div>
	</div>
</section>