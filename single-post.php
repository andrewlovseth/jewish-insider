<?php

/*

	Template used for featured posts

*/

global $theme;
$theme = "overlay";

/*
if(get_field('email_gateway_switch') == TRUE) {
	add_filter( 'body_class','my_body_classes' );
	function my_body_classes( $classes ) {
	 
		$classes[] = 'email-gateway-on hide-content';
		 
		return $classes;
		 
	}
}
*/

get_header(); 

// get_template_part('template-parts/global/email-gateway');

?>

	<article <?php post_class('feature page-content'); ?>>

		<?php get_template_part('template-parts/article/featured-image'); ?>

		<section class="header">
			<div class="header-wrapper">

				<?php get_template_part('template-parts/article/title'); ?>

				<?php get_template_part('template-parts/article/dek'); ?>

				<?php get_template_part('template-parts/article/byline'); ?>

				<?php get_template_part('template-parts/article/share'); ?>

				<?php get_template_part('template-parts/article/dateline'); ?>		

			</div>
		</section>

		<?php get_template_part('template-parts/article/body'); ?>

		<?php get_template_part('template-parts/article/footer'); ?>

	</article>

<?php get_footer(); ?>