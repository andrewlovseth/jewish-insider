		<footer class="site-footer">
			<div class="wrapper">

				<?php get_template_part('template-parts/footer/logo'); ?>

				<div class="footer-utilities">
					<?php get_template_part('template-parts/footer/navigation'); ?>

					<?php get_template_part('template-parts/footer/social'); ?>

					<?php get_template_part('template-parts/footer/subscribe'); ?>
				</div>

				<?php get_template_part('template-parts/footer/copyright'); ?>

			</div>
		</footer>

	</div> <!-- #page -->

	<?php get_template_part('template-parts/global/search-form'); ?>

	<?php get_template_part('template-parts/global/subscribe-overlay'); ?>
	
	<?php wp_footer(); ?>

	<?php the_field('footer_javascript', 'options'); ?>

</body>
</html>