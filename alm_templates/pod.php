<article class="quick-hit page-content" data-url="<?php the_permalink(); ?>">
	<section class="header">
		<div class="header-wrapper">
			<?php get_template_part('template-parts/quick-hit/tagline'); ?>

			<div class="title headline">
				<h1 class="x-large-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
			</div>

			<?php get_template_part('template-parts/article/dek'); ?>

			<?php get_template_part('template-parts/quick-hit/photo'); ?>

			<?php get_template_part('template-parts/article/byline'); ?>

			<?php get_template_part('template-parts/article/share'); ?>

		</div>
	</section>
	
	<section class="body p2">
		<div class="show-more-wrapper">
			<?php the_content(); ?>
		</div>

		<div class="show-more-btn">
			<a href="#" class="subscribe-btn btn">Read More</a>
		</div>		
	</section>
</article>
