<div class="episode">
    <div class="art">
        <div class="content">
            <a href="<?php the_permalink(); ?>">
                <?php if(get_field('show_art')): ?>
                    <img src="<?php $image = get_field('show_art'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                <?php else: ?>
                    <img src="<?php echo $show_art['url']; ?>" alt="<?php echo $show_art['alt']; ?>" />
                <?php endif; ?>
            </a>
        </div>
    </div>

    <div class="info">
        <div class="meta">
            <span class="time"><?php the_time('M j, Y'); ?></span>
        </div>

        <div class="headline">
            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
        </div>

        <div class="dek copy p3">
            <p><?php the_field('dek'); ?></p>
        </div>
    </div>            
</div>